from django.db import models
from django.contrib.auth.models import User
from django.db.models import Sum

# Create your models here.
class Book(models.Model):
    title = models.CharField(max_length = 60)
    price = models.IntegerField(default=0)

    def __str__(self):
      return self.title


class Category(models.Model):
    name = models.CharField(max_length = 60)

    def __str__(self):
        return self.name

class BookCategory(models.Model):
    book = models.ForeignKey(Book , on_delete=models.CASCADE)
    category = models.ForeignKey(Category , on_delete = models.CASCADE)

    # def __str__(self):
    #     return self.book.title

class BookAuthor(models.Model):
    book = models.ForeignKey(Book ,on_delete=models.CASCADE)
    author = models.ForeignKey(User ,on_delete=models.CASCADE)

    # def __str__(self):
    #     return self.author