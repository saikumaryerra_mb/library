from django.core.management.base import BaseCommand , CommandError
from  drills.models import *
import random
from django.contrib.auth.models import User

class Command(BaseCommand):
    help = "to populate database"

    def handle(self,*args,**kwargs):
        for i in range(1,11):
            u=User.objects.create_user('u%s' % i)
            u.set_password = 'u%s' % i
            u.save()
            c = Category.objects.create(name = 'c%s' % i )
            c.save()
        
        for i in range(1,41):
            b=Book.objects.create()
            b.title = 'b%s' % i
            b.price = random.randint(10,100)
            # print(b.price)
            b.save()
            
            categories = random.sample(range(1,11),3)
            authors = random.sample(range(1,11),2)
            for c in categories:
                bc = BookCategory.objects.create(book=b,category = Category(pk=c))
                bc.save()
            
            for a in authors:
                ba = BookAuthor.objects.create(book=b,author=User(pk=a) )
                ba.save()
            
