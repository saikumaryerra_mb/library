from .models import *
from django.db.models import Sum , Max

def fetch_book(book_id):
    return Book.objects.get(pk=book_id)

def fetch_books(book_ids):
    return Book.objects.filter(pk__in = book_ids)

def fetch_first_books(num):
    return Book.objects.filter(pk__lt = num+1)

def fetch_books_with_category(category_name):
    bc = BookCategory.objects.filter(category__name = category_name).values_list('book' , flat = True)
    book_ids = [x for x in bc]
    return Book.objects.filter(pk__in = book_ids)

def author_with_most_books():
    u = User.objects.all()
    max= 0
    for user in u:
        c = BookAuthor.objects.filter(author = user).count()
        if max==0:
            max=c
            max_user = user
        elif max<c:
            max =c
            max_user = user
    return max_user

def total_cost_of_books():
    return Book.objects.aggregate(Sum('price'))['price__sum']

def max_priced_book():
    # return Book.objects.aggregate(Max('price'))['price__max']
    return Book.objects.order_by('-price').first()

def books_price_greaterthan(value):
    return Book.objects.filter(price__gt = value).order_by('-price')

def delete_book(book_id):
    b = Book.objects.get(pk = book_id).delete()
    # b.save()
